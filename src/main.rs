// extern { fn hello(); }

// fn main() {
//     unsafe { hello(); }
// }
#[link(name = "mps")]
extern "C" {
    fn hello();
}

fn main() {
    unsafe { hello(); };
    //println!("3 squared is {}", r);
}